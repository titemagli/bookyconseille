---
title: "Ma page de recommandations"
order: 0
in_menu: true
---
# Les Logiciels que j'utilise à la librairie

  <article class="framalibre-notice">
    <div>
      <img src="https://framalibre.org/images/logo/Mastodon.png">
    </div>
    <div>
      <h2>Mastodon</h2>
      <p>Un logiciel décentralisé et fédéré de microblogage en 500 caractères.</p>
      <div>
        <a href="https://framalibre.org/notices/mastodon.html">Vers la notice Framalibre</a>
        <a href="https://joinmastodon.org/">Vers le site</a>
      </div>
    </div>
  </article>

  <article class="framalibre-notice">
    <div>
      <img src="https://framalibre.org/images/logo/uBlock%20Origin.png">
    </div>
    <div>
      <h2>uBlock Origin</h2>
      <p>Une extension vous laissant le choix de bloquer, ou non, les publicités des sites web que vous rencontrez.</p>
      <div>
        <a href="https://framalibre.org/notices/ublock-origin.html">Vers la notice Framalibre</a>
        <a href="https://github.com/gorhill/uBlock">Vers le site</a>
      </div>
    </div>
  </article>

  <article class="framalibre-notice">
    <div>
      <img src="https://framalibre.org/images/logo/Thunderbird.png">
    </div>
    <div>
      <h2>Thunderbird</h2>
      <p>Célèbre client de courriel issu du projet Mozilla, distribué par la Fondation Mozilla.</p>
      <div>
        <a href="https://framalibre.org/notices/thunderbird.html">Vers la notice Framalibre</a>
        <a href="https://www.thunderbird.net/fr/">Vers le site</a>
      </div>
    </div>
  </article>

  <article class="framalibre-notice">
    <div>
      <img src="https://framalibre.org/images/logo/Tusky.png">
    </div>
    <div>
      <h2>Tusky</h2>
      <p>Un client Mastodon pour Android léger.</p>
      <div>
        <a href="https://framalibre.org/notices/tusky.html">Vers la notice Framalibre</a>
        <a href="https://github.com/tuskyapp/Tusky">Vers le site</a>
      </div>
    </div>
  </article>

  <article class="framalibre-notice">
    <div>
      <img src="https://framalibre.org/images/logo/Inkscape.png">
    </div>
    <div>
      <h2>Inkscape</h2>
      <p>Un puissant logiciel de dessin vectoriel.</p>
      <div>
        <a href="https://framalibre.org/notices/inkscape.html">Vers la notice Framalibre</a>
        <a href="https://inkscape.org/fr/">Vers le site</a>
      </div>
    </div>
  </article>

  <article class="framalibre-notice">
    <div>
      <img src="https://framalibre.org/images/logo/Yakform.png">
    </div>
    <div>
      <h2>Yakform</h2>
      <p>Yakforms vous permet de créer simplement et rapidement des formulaires en ligne.</p>
      <div>
        <a href="https://framalibre.org/notices/yakform.html">Vers la notice Framalibre</a>
        <a href="https://yakforms.org/">Vers le site</a>
      </div>
    </div>
  </article>

  <article class="framalibre-notice">
    <div>
      <img src="https://framalibre.org/images/logo/Hugo.png">
    </div>
    <div>
      <h2>Hugo</h2>
      <p>Hugo est un générateur de site Web HTML et CSS statique écrit en Go . Il est très performant.</p>
      <div>
        <a href="https://framalibre.org/notices/hugo.html">Vers la notice Framalibre</a>
        <a href="https://gohugo.io/">Vers le site</a>
      </div>
    </div>
  </article>



# Dans le cadre associatif

  <article class="framalibre-notice">
    <div>
      <img src="https://framalibre.org/images/logo/B%C3%A9n%C3%A9valibre.png">
    </div>
    <div>
      <h2>Bénévalibre</h2>
      <p>Un logiciel pour gérer et valoriser le bénévolat au sein de votre association.</p>
      <div>
        <a href="https://framalibre.org/notices/b%C3%A9n%C3%A9valibre.html">Vers la notice Framalibre</a>
        <a href="https://benevalibre.org/">Vers le site</a>
      </div>
    </div>
  </article>

  <article class="framalibre-notice">
    <div>
      <img src="https://framalibre.org/images/logo/Framadate.png">
    </div>
    <div>
      <h2>Framadate</h2>
      <p>Organiser des rendez-vous simplement, librement.</p>
      <div>
        <a href="https://framalibre.org/notices/framadate.html">Vers la notice Framalibre</a>
        <a href="https://framadate.org/">Vers le site</a>
      </div>
    </div>
  </article>

  <article class="framalibre-notice">
    <div>
      <img src="https://framalibre.org/images/logo/Signal.png">
    </div>
    <div>
      <h2>Signal</h2>
      <p>Application de messagerie et téléphonie mobile respectueuse de la vie privée.</p>
      <div>
        <a href="https://framalibre.org/notices/signal.html">Vers la notice Framalibre</a>
        <a href="https://signal.org">Vers le site</a>
      </div>
    </div>
  </article>

  <article class="framalibre-notice">
    <div>
      <img src="https://framalibre.org/images/logo/Mattermost.png">
    </div>
    <div>
      <h2>Mattermost</h2>
      <p>Mattermost est un logiciel décentralisé de communication en équipe.</p>
      <div>
        <a href="https://framalibre.org/notices/mattermost.html">Vers la notice Framalibre</a>
        <a href="https://www.mattermost.org/">Vers le site</a>
      </div>
    </div>
  </article>

  <article class="framalibre-notice">
    <div>
      <img src="https://framalibre.org/images/logo/Mumble.png">
    </div>
    <div>
      <h2>Mumble</h2>
      <p>La Voix sur ip libre et décentralisée.</p>
      <div>
        <a href="https://framalibre.org/notices/mumble.html">Vers la notice Framalibre</a>
        <a href="https://wiki.mumble.info/wiki/Main_Page">Vers le site</a>
      </div>
    </div>
  </article>

  <article class="framalibre-notice">
    <div>
      <img src="https://framalibre.org/images/logo/BigBlueButton.png">
    </div>
    <div>
      <h2>BigBlueButton</h2>
      <p>Logiciel de vidéo-conférence spécialisé pour l'apprentissage en ligne</p>
      <div>
        <a href="https://framalibre.org/notices/bigbluebutton.html">Vers la notice Framalibre</a>
        <a href="https://bigbluebutton.org/">Vers le site</a>
      </div>
    </div>
  </article>

  <article class="framalibre-notice">
    <div>
      <img src="https://framalibre.org/images/logo/Etherpad.png">
    </div>
    <div>
      <h2>Etherpad</h2>
      <p>Un éditeur de texte collaboratif et en temps réel !</p>
      <div>
        <a href="https://framalibre.org/notices/etherpad.html">Vers la notice Framalibre</a>
        <a href="http://etherpad.org/">Vers le site</a>
      </div>
    </div>
  </article>

  <article class="framalibre-notice">
    <div>
      <img src="https://framalibre.org/images/logo/Nextcloud.png">
    </div>
    <div>
      <h2>Nextcloud</h2>
      <p>Nextcloud est une plate-forme auto-hébergée de services de stockage et d’applications diverses dans les nuages.</p>
      <div>
        <a href="https://framalibre.org/notices/nextcloud.html">Vers la notice Framalibre</a>
        <a href="https://nextcloud.com/">Vers le site</a>
      </div>
    </div>
  </article>

  <article class="framalibre-notice">
    <div>
      <img src="https://framalibre.org/images/logo/Paheko.png">
    </div>
    <div>
      <h2>Paheko</h2>
      <p>Gestion d'association : membres, compta pro mais simple, cotisations, site web, documents, etc.</p>
      <div>
        <a href="https://framalibre.org/notices/paheko.html">Vers la notice Framalibre</a>
        <a href="https://paheko.cloud/">Vers le site</a>
      </div>
    </div>
  </article>

  <article class="framalibre-notice">
    <div>
      <img src="https://framalibre.org/images/logo/Grisbi.png">
    </div>
    <div>
      <h2>Grisbi</h2>
      <p>Logiciel de comptabilité personnelle intuitif et efficace.</p>
      <div>
        <a href="https://framalibre.org/notices/grisbi.html">Vers la notice Framalibre</a>
        <a href="https://www.grisbi.org/">Vers le site</a>
      </div>
    </div>
  </article>

  <article class="framalibre-notice">
    <div>
      <img src="https://framalibre.org/images/logo/dokuwiki.png">
    </div>
    <div>
      <h2>dokuwiki</h2>
      <p>DokuWiki est un moteur de wiki Libre, modulable, multilingue et sans base de données propulsé par PHP.</p>
      <div>
        <a href="https://framalibre.org/notices/dokuwiki.html">Vers la notice Framalibre</a>
        <a href="https://www.dokuwiki.org/dokuwiki">Vers le site</a>
      </div>
    </div>
  </article>

  <article class="framalibre-notice">
    <div>
      <img src="https://framalibre.org/images/logo/PeerTube.png">
    </div>
    <div>
      <h2>PeerTube</h2>
      <p>PeerTube est un logiciel décentralisé et fédéré d'hébergement de vidéos.</p>
      <div>
        <a href="https://framalibre.org/notices/peertube.html">Vers la notice Framalibre</a>
        <a href="https://joinpeertube.org/fr/">Vers le site</a>
      </div>
    </div>
  </article>
<article class="framalibre-notice">
    <div>
      <img src="https://framalibre.org/images/logo/Evolution.png">
    </div>
    <div>
      <h2>Evolution</h2>
      <p>Evolution : Le client de messagerie qui n'a rien a envier aux autres.</p>
      <div>
        <a href="https://framalibre.org/notices/evolution.html">Vers la notice Framalibre</a>
        <a href="https://wiki.gnome.org/Apps/Evolution/">Vers le site</a>
      </div>
    </div>
  </article>

  <article class="framalibre-notice">
    <div>
      <img src="https://framalibre.org/images/logo/Spip.png">
    </div>
    <div>
      <h2>Spip</h2>
      <p>Très puissant, SPIP peut gérer un petit blog comme un gros site, grâce à son système de « squelettes »...</p>
      <div>
        <a href="https://framalibre.org/notices/spip.html">Vers la notice Framalibre</a>
        <a href="http://www.spip.net/">Vers le site</a>
      </div>
    </div>
  </article>



# Pour le plaisir

  <article class="framalibre-notice">
    <div>
      <img src="https://framalibre.org/images/logo/Superflu%20Returnz.png">
    </div>
    <div>
      <h2>Superflu Returnz</h2>
      <p>Le jeu vidéo inutile du superhéros qui ne sert à rien</p>
      <div>
        <a href="https://framalibre.org/notices/superflu-returnz.html">Vers la notice Framalibre</a>
        <a href="https://studios.ptilouk.net/superflu-riteurnz/">Vers le site</a>
      </div>
    </div>
  </article>

  <article class="framalibre-notice">
    <div>
      <img src="https://framalibre.org/images/logo/Minetest.png">
    </div>
    <div>
      <h2>Minetest</h2>
      <p>Un jeu de cubes dans un espace illimité ou pioches et pelles sont de rigueur pour édifier des constructions...</p>
      <div>
        <a href="https://framalibre.org/notices/minetest.html">Vers la notice Framalibre</a>
        <a href="https://www.minetest.net/">Vers le site</a>
      </div>
    </div>
  </article>

  

  <article class="framalibre-notice">
    <div>
      <img src="https://framalibre.org/images/logo/VLC.png">
    </div>
    <div>
      <h2>VLC</h2>
      <p>VLC est un lecteur multimédia très populaire.</p>
      <div>
        <a href="https://framalibre.org/notices/vlc.html">Vers la notice Framalibre</a>
        <a href="https://www.videolan.org/vlc/">Vers le site</a>
      </div>
    </div>
  </article>

  <article class="framalibre-notice">
    <div>
      <img src="https://framalibre.org/images/logo/Rolisteam.png">
    </div>
    <div>
      <h2>Rolisteam</h2>
      <p>Libérez vos parties (de jeu de rôle) avec Rolisteam.</p>
      <div>
        <a href="https://framalibre.org/notices/rolisteam.html">Vers la notice Framalibre</a>
        <a href="http://www.rolisteam.org">Vers le site</a>
      </div>
    </div>
  </article>

  <article class="framalibre-notice">
    <div>
      <img src="https://framalibre.org/images/logo/K-9%20Mail.png">
    </div>
    <div>
      <h2>K-9 Mail</h2>
      <p>Client de courriel pour Android</p>
      <div>
        <a href="https://framalibre.org/notices/k-9-mail.html">Vers la notice Framalibre</a>
        <a href="https://k9mail.app/">Vers le site</a>
      </div>
    </div>
  </article>

  <article class="framalibre-notice">
    <div>
      <img src="https://framalibre.org/images/logo/NewPipe.png">
    </div>
    <div>
      <h2>NewPipe</h2>
      <p>Client multimédia YouTube, PeerTube, SoundCloud &amp; MediaCCC libre pour Android.</p>
      <div>
        <a href="https://framalibre.org/notices/newpipe.html">Vers la notice Framalibre</a>
        <a href="https://newpipe.schabi.org/">Vers le site</a>
      </div>
    </div>
  </article>

  <article class="framalibre-notice">
    <div>
      <img src="https://framalibre.org/images/logo/Organic%20Maps.svg">
    </div>
    <div>
      <h2>Organic Maps</h2>
      <p>Application libre de cartes hors ligne et navigation GPS basée sur les données OpenStreetMap - Pour Android et iOS</p>
      <div>
        <a href="https://framalibre.org/notices/organic-maps.html">Vers la notice Framalibre</a>
        <a href="https://organicmaps.app/fr/">Vers le site</a>
      </div>
    </div>
  </article>

  <article class="framalibre-notice">
    <div>
      <img src="https://framalibre.org/images/logo/Tarot%20Confin%C3%A9.png">
    </div>
    <div>
      <h2>Tarot Confiné</h2>
      <p>Pour jouer au tarot, comme en vrai, avec ses amis pendant le confinement, et après...</p>
      <div>
        <a href="https://framalibre.org/notices/tarot-confin%C3%A9.html">Vers la notice Framalibre</a>
        <a href="https://github.com/mapariel/tarot_confine/tree/wsversion">Vers le site</a>
      </div>
    </div>
  </article> 